module ID3Tree where

import Data.List
import Data.Ord(comparing)
import DataStructures
import UtilityFunctions

-- Calculate the entropy for a single value of the list, given the the total element sum
entropysingle :: Float -> Float -> Float
entropysingle elementsforval totalelements
    | isNaN $ ent elementsforval totalelements = 0
    | otherwise = ent elementsforval  totalelements
    where 
        px = elementsforval / totalelements
        ent elementsforval totalelements = (px * logBase 2 px)

-- Calculate entropy of a list of elemenets
entropy :: [Float] -> Float
entropy list =
    let
        listtotal = sum list
    in
     -(sum $ map (\x -> entropysingle x listtotal) list)
           
-- Calculate the purity of a given set for a certain attributename and targetname
purity :: Set -> AttributeName -> TargetName-> Float
purity set attributename targetname= 
    let
        domainvalues = getDomainValues set attributename
        targetvalues =  getTargetValues set targetname
        numberofvalues = length domainvalues
        groupedvaluesbydomain = groupBy (\a b -> fst a == fst b) $ sortBy (comparing fst) $ zip domainvalues targetvalues
        groupedvaluesbydomainandtarget = map (groupBy (\a b -> snd a == snd b)) $ map (sortBy (comparing snd)) groupedvaluesbydomain
        -- [[countpertargevalue]]
        countedvalues = map (\x -> map (\y ->fromIntegral $ length y) x) groupedvaluesbydomainandtarget
        entvalues = map (\x -> (entropy x) * (sum x) ) countedvalues
        entvaluesadjusted = map (\x -> x / (fromIntegral numberofvalues)) entvalues
    in
        foldl1 (+) entvaluesadjusted

-- Calculate the set purity for a given targetname
setpurity :: Set -> TargetName -> Float
setpurity set targetname = 
    let 
        targetvalues = getTargetValues set targetname
        grouped = group $ sort targetvalues
        counted = map (\x -> fromIntegral $ length x) grouped
    in
        entropy counted

-- Calculate the maximum information gain for a given targetname
maxInformationGain :: Set -> TargetName -> Float
maxInformationGain set targetname = 
    let 
        setpuritycalc = setpurity set targetname
        attributenames = filter (/=targetname) $ getAttributeNames set
        informationgain = map (\x -> setpuritycalc - (purity set x targetname)) attributenames
        index = argmax informationgain
    in
        head $ drop index informationgain

-- Calculate the attributename best suitable for a split
bestSplit :: Set -> TargetName -> AttributeName
bestSplit set targetname = 
    let 
        attributenames = filter (/=targetname) $ getAttributeNames set
        setpuritycalc = setpurity set targetname
        informationgain = map (\x -> setpuritycalc - (purity set x targetname)) attributenames
        index = argmax informationgain
    in 
        head $ drop index attributenames

-- Split the set on the attribute with the most informationgain
splitSet :: Set -> TargetName -> (AttributeName,[(DomainValue,Set)])
splitSet set targetname
    | setpurity set targetname == 0 = ("",[])
    |otherwise = 
        let 
            attributename = bestSplit set targetname
            domainvalues = unique2 $ getDomainValues set attributename
        in
            (attributename, map (\x -> (x, createSet set attributename x)) domainvalues)

-- Create a new set retaining only the rows wich have a certain domainvalue for a attribute 
createSet :: Set -> AttributeName -> DomainValue -> Set
createSet set attributename domainvalue =
    let
        attributevals = getDomainValues set attributename
        allattributeNames = filter (/= attributename) $ getAttributeNames set
        alldomainvals = map (getDomainValues set) allattributeNames
        alldomainvalsuntransposed = transpose alldomainvals
        alldomainvalsuntransposedfiltered = map (snd) $ filter (\x -> fst x == domainvalue) $ zip attributevals alldomainvalsuntransposed
        alldomainvalsfiltered = transpose alldomainvalsuntransposedfiltered
    in
        zip allattributeNames alldomainvalsfiltered

-- Recursive loop for constructing tree
buildTree :: TargetName -> DomainValue -> Set -> Tree Set
buildTree targetname domainvalue set 
    -- Leaf node, setpurity is 0
    | setpurity set targetname == 0 = 
        let
            targetvalue = head $ getTargetValues set targetname
        in
            Leaf domainvalue targetvalue set
    -- Uncertainleaf, non-deterministic path was found and handled
    | (length set == 1) || (maxInformationGain set targetname == 0)=
        let
            targetvalues = getTargetValues set targetname
            totaltargetvalues = fromIntegral $ length targetvalues
            targetvaluesgrouped = group $ sort $ targetvalues
            targetvaluescounted = map (\x -> (head x, (fromIntegral $ length x)/totaltargetvalues)) targetvaluesgrouped
        in
            UncertainLeaf domainvalue targetvaluescounted set
    -- Node, construct recursive structure 
    | otherwise = 
        let 
            splitset = splitSet set targetname
            attributename = fst splitset
            newsets = snd splitset
        in
            Node domainvalue attributename (map (\x -> buildTree targetname (fst x) (snd x)) newsets) set

-- Call buildtree with initial arguments
id3Tree :: Set -> TargetName -> Tree Set
id3Tree set targetname =
    let
        domainvalue = "root"
    in
        buildTree targetname domainvalue set

