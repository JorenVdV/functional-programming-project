module Main where

import DataStructures
import UtilityFunctions
import ID3Tree

main :: IO()
main = do
    -- Read dataset from csv file
    print "Reading dataset"
    dataset <- readCsv "weather.csv"
    -- Construct ID3 Tree 
    print "Constructing ID3 tree"
    let tree = id3Tree dataset "Play"
    -- Construct dot structure string for image
    print "Consctructing dot string"
    let dottree = graphvizTree tree
    -- Write dot file
    print "Writing dot file"
    writeFile  "weather.dot" dottree
    return ()